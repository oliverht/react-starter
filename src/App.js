import React, { PureComponent } from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Component from "./components/Component";

import rootReducer from "./reducers";
const store = createStore(rootReducer);

export default class App extends PureComponent {
	render() {
		return (
			<Provider store={store}>
				<Router>
					<Route path="/" component={Component} />
				</Router>
			</Provider>
		);
	}
}
